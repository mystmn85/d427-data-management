## Examples

SELECT aa.street, aa.zipcode, a.first_name, a.last_name, a.actor_id
FROM actor as a
RIGHT JOIN actor_address as aa
ON aa.actor_id = a.actor_id
ORDER BY a.actor_id;

SELECT * FROM actor_address;

UPDATE actor_address
SET actor_id = '123 Macfie St.'
WHERE actor_id = 5; 

INSERT INTO actor_address (street, zipcode, address_actor, actor_id, today_date)
VALUES
(concat(CEILING(RAND()*1000),' Example St.'), CEILING(RAND()*1000)+ '47', 7, 7, now());