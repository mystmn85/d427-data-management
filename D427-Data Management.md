# *Section-1*

## 1.1 *Different terms*
***File*** -> Table </br>
***Record*** ->Row</br>
***Attribute*** -> Col </br>
***Tuple*** -> Row </br>
***Domain*** -> Data Type

# *Commands*
## 1.3
+ Show databases;
+ Show tables;
+ Show columns from [table]
+ Show create tabke [table]

## 1.4 - Alter Table
+ ***Add*** <span style="color:lightblue">ALTER TABLE</span> TableName 
<span style="color:lightblue">ADD</span>  ColumnName DataType;

+ ***Change*** <span style="color:lightblue">ALTER TABLE</span> TableName <span style="color:lightblue">CHANGE</span>  CurrentColumnName NewColumnName NewDataType;
  
+ ***DROP*** <span style="color:lightblue">ALTER TABLE</span> TableName <span style="color:lightblue">DROP</span> ColumnName;

***
## 1.7 - NULL / NOT NULL OPERATOR 
+ ...WHERE [column1] =  NULL 

***
## 1.8 - INSERT / UPDATING / DELETE 
***UPDATE statement modifies existing rows in a table.***  
+ UPDATE [<span style="color:lightblue">TABLE</span>] SET [<span style="color:lightblue">column_name</span>] = [<span style="color:lightblue">value</span>] WHERE [<span style="color:lightblue">column_name</span>] = [<span style="color:lightblue">value</span>]

***DELETE existing rows in a table.***  
+ DELETE FROM [table] WHERE [<span style="color:lightblue">column_name</span>] = [<span style="color:lightblue">value</span>]

***TRUNCATE statement deletes all rows from a table***  
+ TRUNCATE TABLE [<span style="color:lightblue">TABLE</span>]

***
## 1.9 - PRIMARY KEYS
***column, or group of columns, used to identify a row. A solid circle (●) precedes the primary key*** 

CREATE TABLE Employee ( </br>
   ID        SMALLINT UNSIGNED, </br>
   Name      VARCHAR(60), </br>
   Salary    DECIMAL(7,2), </br>
   <span style="color:lightblue">PRIMARY KEY  (COLUMN_NAME)</span> </br>
);

***
## 1.12 - CONSTRAINTS
***Constraints are added and dropped with the ALTER TABLE TableName followed by an ADD, DROP, or CHANGE clause.***
+ _[ <span style="color:orange">CHANGE, ADD, DROP</span> ]_ CurrentColumnName NewColumnName NewDataType[ <span style="color:lightblue">PRIMARY KEY, FOREIGN KEY, UNIQUE, CHECK</span> ]
+ Example:
+ + ADD [CONSTRAINT ConstraintName] FOREIGN KEY
+ + DROP FOREIGN KEY ConstraintName

***
## 2.1 - Special operators and clauses
***IN***
+ ..WHERE Code IN ('AGO', 'Aruba');

***BETWEEN***
+ ..WHERE Code BETWEEN '2000' AND '2001';

***LIKE***
+ ..WHERE CountryCode LIKE 'A_W';
+ + <span style="color:orange">_ matches exactly one character</span>
+ ..WHERE Language LIKE 'A%n';
+ + <span style="color:orange">% matches any number of characters.</span>
+ ..WHERE Language LIKE 'A%';
+ <span style="color:lightblue">%star%</span>
+ + Shows all Star queries

***DISTINCT*** (_return only unique or 'distinct' values_)
+ SELECT <span style="color:orange">DISTINCT</span> [column name]

***ORDER BY*** (_ascending or descending_)
+ ..ORDER BY [column-1], [column-2]

***
## 2.2 - Numeric functions
***function*** (_operates on an expression enclosed in parentheses, called an argument, and returns a value_)
+ RAND()
+ LOWER('MySQL')
+ SELECT SUBSTRING('Boomerang', 1, 4)
+ +  returns 'Boom'
+ NOW()
+ ..WHERE YEAR(ReleaseDate) > '2017' OR MONTH(ReleaseDate) = '11';

***GROUP BY clause*** (_from a set of rows and returns a summary value_)
+ COUNT()
+ MIN()
+ AVG()
+ ex: <span style="color:lightblue"></br>WHERE (C.State = 'CO' OR C.State = 'OK') </br>AND Month(S.Date) = 2 </br>AND Year(S.Date) = 2020</span>


***aggregate function*** (_each simple or composite value of the column(s) becomes a group_)
+ appears between the WHERE clause, if any, and the ORDER BY clause
+ ignores <span style="color:orange">NULL</span> values

***HAVING clause*** (_is used with the GROUP BY clause to filter group results_)
+ GROUP BY CountryCode
HAVING SUM (Population) > 2300000;
+ ex:
+ + _SELECT Genre, COUNT(*), MAX(ReleaseYear)
FROM Song
GROUP BY Genre
HAVING COUNT(*) > 1;_

***
## 2.4 - JOIN queries
***Joins*** (_operates on an expression enclosed in parentheses, called an argument, and returns a value_)

***AS*** (_column name can be replaced with an alias_)

***Inner and full joins*** (_column name can be replaced with an alias_)
+ <span style="color:orange">INNER JOIN</span> selects only matching left and right table rows.
+ + ex: <span style="color:lightblue">SELECT C.State, B.ID, B.Title, S.Quantity, (S.Quantity * S.UnitPrice)</br>
FROM Customer as C</br>
INNER JOIN Sale as S on S.CustID = C.ID</br>
INNER JOIN Book as B ON B.ID = S.BookID;</span>
+ <span style="color:orange">FULL JOIN</span> selects all left and right table rows, regardless of match.
+ <span style="color:orange">OUTER JOIN</span> selects unmatched rows, including left, right, and full joins.
+ <span style="color:orange">LEFT JOIN</span> selects all left table rows, but only matching right table rows.
+ <span style="color:orange">RIGHT JOIN</span> selects all right table rows, but only matching left table rows.
+ The <span style="color:orange">ON</span> clause specifies the join columns. 

***Alternative join queries*** (_JOIN clause clarifies join behavior and simplifies queries._)
+ <span style="color:orange">UNION</span> combines the two results into one table. 

***
## 2.5 - Equijoins, self-joins, and cross-joins
***Equijoins*** (_compares columns of two tables with the = operator._)

***Self-joins*** (_joins a table to itself_)
+  compare any columns of a table, as long as the columns have comparable data types
+  ex:
+  + SELECT A.Name, B.Name
FROM EmployeeManager A
INNER JOIN EmployeeManager B
ON B.ID = A.Manager;

***Cross-joins*** (_combines two tables without comparing columns_)
+ compare any columns of a table, as long as the columns have comparable data types
+ all possible combinations of rows from both tables appear in the result.

***
## 2.6 - Subqueries
***subquery*** (_nested query or inner query, is a query within another SQL query._)
+ ex:
+ + SELECT Language
FROM CountryLanguage
WHERE Percentage < 
    (SELECT Percentage
    FROM CountryLanguage
    WHERE Language = 'Mbundu');

***Correlated subqueries*** (_when the subquery's WHERE clause references a column from the outer query_)
+ ex:
+ + SELECT Name, CountryCode FROM City WHERE 2 <= (SELECT COUNT(*) FROM CountryLanguage WHERE CountryCode = City.CountryCode);

***
## 2.7 - Complex query example
***Joining tables*** (_when the subquery's WHERE clause references a column from the outer query_)
+ ex: </br><span style="color:orange">
  SELECT C.State, S.BookID, B.Title, SUM(S.Quantity) AS Quantity, SUM(S.UnitPrice * S.Quantity) AS TotalSales </br>
FROM Sale S </br>
INNER JOIN Customer C ON C.ID = S.CustID </br>
INNER JOIN Book AS B ON B.ID = S.BookID </br>
WHERE (C.State = 'CO' OR C.State = 'OK') AND MONTH(S.Date) = 2 </br> AND YEAR(S.Date) = 2020 AND B.ID IN </br>
 (<span style="color:lightblue">SELECT BookID
 FROM BookAuthor
 Group By BookID
 HAVING COUNT(*) = 1</span>)</br>
GROUP BY C.State, S.BookID</br>
ORDER BY TotalSales DESC;</span>

## 2.8 - View tables
***Creating views*** (_Views restructure table columns and data types without changes to the underlying database design_)
+ <span style="color:orange">CREATE VIEW [View Name]</br>
  AS SELECT [col1, col2, col3]</br>
  FROM [table]
  WHERE [col1] = [col2]
  </span>
+ View INSERT or UPDATE are able to use _CHECK_
+ ..<span style="color:orange">WHERE Faculty.Code = Department.Code</br>
 CHECK OPTION;</span>

 ## 2.11 - LAB Manager with INNER JOIN
 ***INNER JOIN***
 + ex..<span style="color:orange">
 Select E.FirstName as Employee, M.FirstName as Manager</br>
From Employee as E</br>
INNER JOIN Employee as M on M.ID = E.ManagerID</br>
ORDER BY E.FirstName;</span>

 ## 2.12 - LAB Manager Lesson with INNER JOIN
 ***INNER JOIN***
+ ex.. <span style="color:orange">SELECT l.LessonDateTime, l.HorseID, s.FirstName, s.LastName</br>
FROM LessonSchedule as l</br>
INNER JOIN Horse as h</br>
INNER JOIN Student as s</br>
ON l.HorseID = h.ID</br>
AND l.StudentID = s.ID</br>
ORDER BY LessonDateTime asc, l.HorseID;</span>

***
## 4.3 - Cardinality
***Relationship maximum***
+ .. is the greatest number of instances of one entity that can relate to a single instance of another entity.

***Relationship minimum***
+ is the least number of instances of one entity that can relate to a single instance of another entity.

***Attribute maximum and minimum***
+ 1(1) - singular  <span style="color:lightgreen">required</span>
+ 1(0) - singular  <span style="color:lightpink">optional</span>
+ M(0) - pluar <span style="color:yellow">optional</span>

***Unique attributes***
_1 indicates a unique attribute and M indicates a non-unique attribute_
+ 11(1) <span style="color:#c5f015">unique</span>
+ 11(0) <span style="color:#c5f015">unique</span>
+ M1(1) <span style="color:#c54015">not unique</span>
+ MM(0) <span style="color:#c54015">not unique</span>

+ 1-1(1)- t,p,f
+ M-M(1) - t,p
+ 1-1(1) - 
+ M-1(1) - 
  
## 7.3 LAB - Update rows in Horse table
***ex: updating row values***
+ <span style="color:#bd4e4e
">UPDATE Horse SET height = '15.6' WHERE ID = 2;</span>
+ <span style="color:#ca7171">UPDATE Horse SET RegisteredName = 'Lady Luck', BirthDate = '2015-05-01' WHERE ID = 4; </span>
+ <span style="color:#dea6a6">UPDATE Horse SET Breed = NULL WHERE BirthDate >= '2016-12-22';</span>

## 7.4 LAB - Delete rows from Horse table
***ex: delete row values***
+ <span style="color:#b0e0e6 ">DELETE FROM HORSE WHERE ID=5;</span>
+ <span style="color:#8cb3b8 ">DELETE FROM HORSE WHERE Breed='Holsteiner' OR BREED='PAINT';</span>
+ <span style="color:#69868a">DELETE FROM Horse WHERE BirthDate < '2013-03-13';</span>

## 7.5 LAB - Select horses with logical operators
+ <span style="color:#b0e0e6 ">SELECT RegisteredName, Height, BirthDate</br> FROM Horse</br>WHERE Height BETWEEN '15.0' AND '16.0' </br>OR BirthDate >= '2020-01-01';</span>

## 7.6 LAB - Create LessonSchedule table with FK constraints
+ <span style="color:#bb4e4e ">CREATE TABLE Horse(</br>
   ID SMALLINT UNSIGNED AUTO_INCREMENT,</br>
   RegisteredName VARCHAR(15),</br>
   Primary Key (ID)</br>
);</span>
+ <span style="color:#da4e4e ">CREATE TABLE Horse(</br>
Create TABLE Student(</br>
   ID SMALLINT UNSIGNED AUTO_INCREMENT,</br>
   FirstName VARCHAR(15),</br>
   LastName VARCHAR(15),</br>
   Primary Key (ID)</br>
);</br>
+ <span style="color:#fd4e4e ">CREATE TABLE Horse(</br>
CREATE TABLE LessonSchedule (</br>
   HorseID SMALLINT UNSIGNED NOT NULL,</br>
   StudentID SMALLINT UNSIGNED,</br>
   LessonDateTime DATETIME NOT NULL,</br>
   PRIMARY KEY (HorseID,LessonDateTime),</br>
   FOREIGN KEY (HorseID) REFERENCES Horse(ID)</br>
   ON DELETE CASCADE,</br>
   FOREIGN KEY (StudentID) REFERENCES Student(ID)</br>
   ON DELETE SET NULL</br>
);</span>

## 7.7 LAB - Create Horse table with constraints
+ <span style="color:#b0e0e6 ">Create TABLE Horse(</br>
   ID SMALLINT UNSIGNED AUTO_INCREMENT Primary KEY,</br>
   RegisteredName VARCHAR(15) NOT NULL,</br>
   Breed VARCHAR(20) CHECK(Breed IN ("Egyptian Arab", "Holsteiner", "Quarter Horse", "Paint", "Saddlebred")),</br>
   Height DECIMAL(3,1) CHECK (HEIGHT BETWEEN 10.0 AND 20.0),</br>
   BirthDate DATE CHECK( BirthDate >= '2015-01-01')</br>
);</span>

## 7.8 LAB - Create Student table with constraints
+ <span style="color:#a65eea ">CREATE TABLE Student (</br>
   ID SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,</br>
   FirstName VARCHAR(20) NOT NULL,</br>
   LastName VARCHAR(30) NOT NULL,</br>
   Street VARCHAR (50) NOT NULL,</br>
   City VARCHAR(20) NOT NULL,</br>
   State CHAR(2) NOT NULL DEFAULT "TX",</br>
   Zip mediumint unsigned NOT NULL,</br>
   Phone CHAR(10) NOT NULL,</br>
   Email VARCHAR(30) UNIQUE</br>
);</span>

## 7.9 LAB - Create Movie table 
+  <span style="color:#ea605e ">CREATE TABLE Movie(</br>
   ID SMALLINT UNSIGNED CHECK(ID <=50000),</br>
   Title VARCHAR(50),</br>
   Rating CHAR(4),</br>
   ReleaseDate date,</br>
   Budget DECIMAL (8,2)</br>
);</span>

## Needs WORK
+ INDEX
+ + <span style="color:#b0e0e6 ">CREATE TABLE Example(</br>
  [column1] INT Primary Key,
  [column2] INT NOT NULL,
  [column3] INT NOT NULL,
  INDEX (column2, column3)
  );</span>
+ <span style="color:#bd4e4e">CREATE INDEX [name]</br>
    ON [table_name]</br>
    ([column_name])</span>
+ ex: CREATE INDEX idx_year
ON Movie
(Year);

+ VIEW
+ Which restriction applies when using a materialized view?


https://learn.zybooks.com/zybook/WGUD427v2/chapter/7/section/5

## 2, 4.3, 4.6-4.10, all labs in 7